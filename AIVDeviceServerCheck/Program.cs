﻿
using System;
using System.Xml;
using System.Collections.Generic;
using Canberra.ABACOSApex.Communication;
using Canberra.ABACOSApex.Communication.Dto;
using Canberra.ABACOSApex.Facade.Communication;
using Canberra.ABACOSApex.Communication.Session;
using Canberra.ABACOSApex.BusinessLayer.Session;
using System.Runtime.Remoting.Messaging;
using Canberra.ABACOSApex.Framework;
using System.Net;
using System.Net.Sockets;

namespace AIVDeviceServerCheck
{
    class Program
    {
        static void Main(string[] args)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\temp\InVivo.txt"))
            {
                //XmlDataDocument xmlDoc = new XmlDataDocument();
                //System.IO.FileStream fs = new System.IO.FileStream(@"C:\Program Files (x86)\Canberra\Apex-InVivo\Canberra.Apex-InVivo.DeviceServer.exe.config", System.IO.FileMode.Open, System.IO.FileAccess.Read);
                //xmlDoc.Load(fs);
                //var root = xmlDoc.DocumentElement;
                //var ht = root.GetElementsByTagName("DatabaseName");
              
                // Print the run/stop status of the Device Service
                // must be run locally
                string url = "tcp://127.0.0.1:50030/SystemControllerService";
                ISystemControllerService proxy;

                try
                {
                    proxy = (ISystemControllerService)Activator.GetObject(typeof(ISystemControllerService), url);
                    var details = proxy.GetServiceDetails("Apex-InVivo Device Server");
                   file.WriteLine("Device Server Status - {0}", details[1]);
                }
                catch (Exception e)
                {
                    file.WriteLine(e.Message);
                }

                // write out the databases
                try
                {
                    IUserManagementService management = ServiceManager.Instance.GetUserManagementService();
                    IList<string> dbs = management.ReadDatabaseList();
                    for (var i = 0; i < dbs.Count; i++)
                        file.WriteLine("database - {0}", dbs[i]);
                }
                catch (Exception e)
                {
                   file.WriteLine(e.Message);
                }

                try
                {
                    // Print the Device Server URI
                    IDeviceService deviceService = ServiceManager.Instance.GetDeviceService();
                    file.WriteLine("Device Server URI - {0}", deviceService.GetDeviceServerURI(Environment.MachineName));

                    // Print all the Device Servers
                    IList<string> deviceServers = deviceService.GetAllHostDeviceServers();
                    for (var i = 0; i < deviceServers.Count; i++)
                        file.WriteLine("Server - {0}", deviceServers[i]);

                    // Print the Device Server isRunning
                    file.WriteLine("Is running - {0}", deviceService.IsRunning());
                }
                catch (Exception e)
                {
                    file.WriteLine(e.Message);
                }

                try
                {
                    ICounterService counter = ServiceManager.Instance.GetCounterService();
                    IList<string> nodes = counter.GetAvailableNodes();
                    for (var i = 0; i < nodes.Count; i++)
                        file.WriteLine("Node - {0}", nodes[i]);
                }
                catch (Exception e)
                {
                    file.WriteLine(e.Message);
                }

                // Output the CI_ABS_DETECTOR table
                try
                {
                    ICounterService counter = ServiceManager.Instance.GetCounterService();
                    DetectorDto detectors = counter.GetDetectorsByHost(Environment.MachineName);
                    detectors.WriteDto(file);
                }
                catch (Exception e)
                {
                    file.WriteLine(e.Message);
                }

                // Output the CI_ABS_COUNTER table
                try
                {
                    ICounterService counter = ServiceManager.Instance.GetCounterService();
                    CounterDto counters = counter.GetAllCounters();
                    counters.WriteDto(file);
                }
                catch (Exception e)
                {
                    file.WriteLine(e.Message);
                }

                // Create a dummy ticket
                try
                {
                    AbacosId dummyUserId = AbacosId.NewAbacosId();
                    string dummyUserName = "Apex-InVivo-test";
                    string referenceDbName = "APEX_INVIVO";
                    SessionTicket ticket = SessionTicket.CreateSessionTicket(dummyUserId, dummyUserName, referenceDbName);
                    if (ticket != null)
                        file.WriteLine("Created ticket successfully");
                    else
                        file.WriteLine("Create ticket failed");
                }
                catch (Exception e)
                {
                    file.WriteLine(e.Message);
                }

                // Connect to ports on local box via hostname
                int[] ports = new int[] { 50001, 50020, 50030 };
                foreach (int port in ports)
                {
                    using (TcpClient client = new TcpClient())
                    {
                        try
                        {
                            client.Connect(Environment.MachineName, port);
                            file.WriteLine("Connection to port " + port + " successfull");

                        }
                        catch (Exception e)
                        {
                            file.WriteLine("Connection to port " + port + " un-successfull");
                            file.WriteLine(e.Message);
                        }
                        client.Close();
                    }
                }

                // Create a DeviceServiceImpl Service 
                try
                {
                    string uri = "tcp://" + Environment.MachineName + ":50020/DeviceServerManager";
                    IDeviceServerManager m_proxy = (IDeviceServerManager)Activator.GetObject(typeof(IDeviceServerManager), uri);
                   file.WriteLine("Creation of DeviceServiceImpl Service successfull");
                }
                catch (Exception e)
                {
                   file.WriteLine("Creation of DeviceServiceImpl Service un-successfull");
                   file.WriteLine(e.Message);
                }

            }

            //file.ReadKey();
        
        }
    }
}
